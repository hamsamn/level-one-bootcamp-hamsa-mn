//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float input()
{
    float a;
    scanf("%f",&a);
    return a;
}
float volume(float h,float d,float b)
{
    float vol = (h*d*b)/3+(d/b)/3;
    return vol;
}
void output(float h,float d,float b)
{
    printf("volume of the tromboloid is %f\n",volume(h,d,b));
}
int main()
{
    int h,d,b;
    printf("Enter h");
    h = input();
    printf("Enter d");
    d = input();
    printf("Enter b");
    b = input();
    output (h,d,b);
    return 0;
}